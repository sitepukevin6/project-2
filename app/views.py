from django.shortcuts import render
from django.http import HttpResponse
from app.models import notes

def home(request):
    return render(request, 'home.html')

def new(request):
    content = request.POST.get('Note', '')
    if content != '':
        new_note = notes()
        new_note.content = content
        new_note.save()
    return render(request, 'new note.html', context={'note': content})

def list(request):
    all_notes = notes.objects.all().values()
    id = request.POST.get('delete', 0)
    if id != 0:
        id = int(id[:-1])
    notes.objects.filter(id=id).delete()
    return render(request, 'notes.html', context={'all_notes': all_notes})