# Generated by Django 4.1.2 on 2022-11-27 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_alter_notes_content'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notes',
            name='content',
            field=models.CharField(default=False, max_length=200),
        ),
    ]
